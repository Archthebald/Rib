import { ClientStore } from 'rib-store'

let appStore = new ClientStore({
    name: null,
    theme: {
        color: 'blue'
    }
})

export default appStore